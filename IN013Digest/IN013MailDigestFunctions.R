rm(list = ls())
TIMESTAMPSALARM1 = NULL
TIMESTAMPSALARM2 = NULL
ltcutoff = 0.001
yld = c(252,220.5)
GSIGLOB = 1
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath,no)
{
	print(paste('file path is',filepath))
	print(paste('write-file path is',writefilepath))
	{
	if(no==1)
	TIMESTAMPSALARM1 <<- NULL
	else if(no==2)
	TIMESTAMPSALARM2 <<- NULL
	}
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
	if(no==1)
	{
	gsiidx = ncol(dataread)-1
	tmodidx = ncol(dataread)
	}
	idx1en = 39
	idx2en = 15
	if(nrow(dataread) < 1)
	{
	  print('Err in file')
		return(NULL)
	}
	dataread2 = dataread
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idx1en])),]
	lastt = NA
	lastr = NA
	{
	if(nrow(dataread) < 1)
	{
	 Eac1 = 0
	}
	else{
	print(paste('first reading',as.numeric(dataread[1,idx1en])))
	print(paste('last reading',as.numeric(dataread[nrow(dataread),idx1en])))
	Eac1 = format(round((as.numeric(dataread[nrow(dataread),idx1en]) - as.numeric(dataread[1,idx1en]))/1000,1),nsmall=1)
	lastr = round((as.numeric(dataread[nrow(dataread),idx1en])/1000),3)
	lastt = as.character(dataread[nrow(dataread),1])
	}
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idx2en])),]
  {
	if(nrow(dataread) < 1){
	 Eac2 = 0}
	else{
	print(paste('length of eac2',nrow(dataread)))
	Eac2 = format(round(sum(as.numeric(dataread[,idx2en]))/60000,1),nsmall=1)
	}
	}
	if(no==1)
	{
	GSI = NA
	TMOD=NA
	dataread = as.numeric(dataread2[complete.cases(as.numeric(dataread2[,gsiidx])),gsiidx])
	if(length(dataread))
		GSI = round(sum(dataread)/60000,2)
	dataread = as.numeric(dataread2[complete.cases(as.numeric(dataread2[,tmodidx])),tmodidx])
	if(length(dataread))
		TMOD = round(mean(dataread),1)
	GSIGLOB <<- GSI
	}
	dataread = dataread2
  datewrite = substr(as.character(dataread[1,1]),1,10)
	DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idx2en])),]
  missingfactor = 480 - nrow(dataread2)
	print(paste('missing factor is',missingfactor))
  dataread2 = dataread2[as.numeric(dataread2[,idx2en]) < (ltcutoff*1000),]
	DSPY = round(as.numeric(Eac1)/yld[no],2)
	PR = round(DSPY*100/GSIGLOB,1)
	if(length(dataread2[,1]) > 0)
	{
			{
			if(no==1)
				TIMESTAMPSALARM1 <<- as.character(dataread2[,1])
			else if(no==2)
				TIMESTAMPSALARM2 <<- as.character(dataread2[,1])
			}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/4.8,1),nsmall=1)
	print(paste("No is",no))
	{
	if(no==1)
	{
	print(paste("Writing to",writefilepath))
  df = data.frame(Date = datewrite, Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield = DSPY,LastTime = lastt,
									LastRead = lastr,Gsi=GSI,PR=PR,Tmod=TMOD,stringsAsFactors=F)
  }
	else if(no==2)
	{
	print(paste("Writing to",writefilepath))
  df = data.frame(Date = datewrite, Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield = DSPY,LastTime = lastt,LastRead = lastr,PR=PR,stringsAsFactors=F)
	}
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,filepathm2,writefilepath)
{
  defaultreturn = c(NA,NA)
	dataread1 =try(read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F),silent=T)
  if(class(dataread1)=='try-error')
		return(defaultreturn)
		print(paste('Read',filepathm1))
	if(!is.null(filepathm2))
	{	
		dataread2 =try(read.table(filepathm2,header = T,sep="\t",stringsAsFactors=F),silent=T)
  	#if(class(dataread2)=='try-error')
		#	return(defaultreturn)
  	if(class(dataread2)!='try-error')
		print(paste('Read',filepathm2))
	}

  Eac1T = as.numeric(dataread1[,2])
  Eac2T = as.numeric(dataread1[,3])
  Eac21T=Eac22T=DSPM2=LT2=LR2=NA
	if(!is.null(filepathm2))
	{
  	if(class(dataread2)!='try-error')
  {
	Eac21T = as.numeric(dataread2[,2])
  Eac22T = as.numeric(dataread2[,3])
	DSPM2 = as.numeric(dataread2[,6])
	LT2 = as.character(dataread2[,7])
	LR2 = as.numeric(dataread2[,8])
	}
	}
  df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),EacTotMethod1M1 = Eac1T,EacTotMethod2=Eac2T,
	DailySpecYieldM1 = as.character(dataread1[,6]),LastTimeM1 = as.character(dataread1[,7]),LastReadM1 = as.character(dataread1[,8]),
	Gsi = as.character(dataread1[,9]),PR=as.character(dataread1[,10]),Tmod=as.character(dataread1[,11]),EacTotMethod1M2 = Eac21T,
	EacTotMethod2M2 = Eac22T,DailySpecYieldM2=DSPM2,LastTimeM2=LT2,LastReadM2=LR2,stringsAsFactors=F)
	{
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
  return(c(as.numeric(Eac1T),as.numeric(Eac2T)))
}

