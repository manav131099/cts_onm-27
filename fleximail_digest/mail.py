import imaplib
import email
import smtplib
import email.header
from datetime import datetime, timedelta

# Connect to an IMAP server
def connect(server, user, password):
    m = imaplib.IMAP4_SSL(server)
    t=m.login(user, password)
    m.select('INBOX')
    k=t[0]
    result=[m]
    result.append(k)
    return result

# Download all attachment files for a given email
def downloaAttachmentsInEmail(m, emailid, outputdir):
    today = datetime.today()
    cutoff = today - timedelta(days=1)
    resp1, items = m.search(None, 'FROM', '%s' % emailid,'SINCE','%s' % cutoff.strftime('%d-%b-%Y'))
    items = items[0].split()
    for id in items:
        resp1, data = m.fetch(id, "(BODY.PEEK[])")
        email_body = data[0][1]
        mail = email.message_from_string(email_body)
        if mail.get_content_maintype() != 'multipart':
            return
        for part in mail.walk():
            if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is not None:
                msg = email.message_from_string(email_body)
                decode = email.header.decode_header(msg['Subject'])[0]
                subject= unicode(decode[0])
                s=subject
                filename="Digest["+s[10:17]+"]_"+s[-10]+s[-9]+s[-8]+s[-7]+s[-6]+s[-5]+s[-4]+s[-3]+s[-2]+s[-1]+".xls"
                file=outputdir + filename
                open(file, 'wb').write(part.get_payload(decode=True))

# Download all the attachment files for all emails in the inbox.
def downloadAllAttachmentsInInbox(server, user, password, outputdir):
    m = connect(server, user, password)
    resp, items = m.search(None, "(ALL)")
    items = items[0].split()
    for emailid in items:
        downloaAttachmentsInEmail(m, emailid, outputdir)

def mail(body,ser,send,rec,sub,passwd):
    #SERVER = 'smtp-mail.outlook.com'
    #SERVER = 'smtp.gmail.com'
    SERVER=ser
    FROM = send
    TO = rec
    password=passwd
    SUBJECT = sub
    TEXT = body
    # Actual message
    message = """From: %s\r\nTo: %s\r\nSubject: %s\r\n\

    %s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)

    # Send the mail
    server = smtplib.SMTP(SERVER, 587)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(FROM, password)
    server.sendmail(FROM, TO, message)
    server.close()


def log_write(body,file):
    open(file, 'wb').write(body)
