rm(list = ls())

#Site Mt Last_Probe Last_Fire Count

#Method:
# Read Stns_Meters data for row order (present in aggregate file)

#Conditons
#1. If MFM is off, don't fire inverter alarms
#2. Fire max 5 alarms a day for site

#To-do
# 1. Reset
# 2. Timestamp condition between 9AM - 5PM

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data"
pathWrite = "/home/admin/Start/FlexiAlert.txt"

substType = function(site,substn)
{
	
}

notMeterFault = function(site,substn)
{

}

checkConditionFire = function(site, currTime,tzUse,substn)
{
	if(file.exists(pathWrite))
	{
		data = read.table(pathWrite, header=T,sep="\t",stringsAsFactors=F)
		stns = as.character(data[,1])
		mts = as.character(data[,2])
		stns = paste(snts,mts)
		idxmtch = match(paste(site,substn),stns)
		if(is.finite(idxmtch))
		{
			fireCount = as.numeric(data[idxmtch,5])
			t1 = strptime(as.character(data[idxmtch,4]),format="%Y-%m-%d %H:%M:%S",tz=tzUse)
			t2 = strptime(as.character(currTime),format="%Y-%m-%d %H:%M:%S",tz=tzUse)
			differencemins = as.numeric(difftime(t2,t1,units="mins"))
			fireCondition = 0
			siteType = substType(site,substn)
			if(siteType == "Inverter" && notMeterFault(site,substn) && fireCount < 5)
			if(differencemins > 60)
				fireCondition = 1
		}
	}
}

stns = dir(path)

for(x i 1 : length(stns))
{
	cntry = substr(stns[x],2,3)
	{
		if(cntry == "IN")
			tzUse = "Asia/Calcutta"
		else if(cntry == "MY" || cntry == "SG")
			tzUse = "Asia/Singapore"
		else if(cntry == "VN" || cntry == "TH" || cntry == "KH")
			tzUse = "Asia/Ho_Chi_Minh"
	}
	timeNow = as.character(format(Sys.time(),tz=tzUse))
	checkConditionFire(substr(stns[x]2,7),timeNow,tzUse)
}
