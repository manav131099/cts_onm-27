rm(list=ls(all =TRUE))
library(ggplot2)

pathWrite <- "C:/Users/talki/Desktop/cec intern/results/716/WM/"
result <- read.csv("C:/Users/talki/Desktop/cec intern/results/716/[716]_wm_summary.csv")
graph3 <- "C:/Users/talki/Desktop/cec intern/results/716/WM/SG 716 doubleplot.pdf"

#manually add in x axis labels for last graph (monthly)
monthslabel <- c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec","Jan", "Feb","Mar", "Apr", 'May','Jun')

x <- 3.6 # size of text on plots
rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
result <- result[,-1]
result <- result[-1,]
colnames(result) <- c("date","da","pts","gsi","tamb","hamb")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- as.numeric(paste(result[,6]))

#gsi in the first day is an error
result[1,4] <- 0

date <- result[,1]

dagraph <- ggplot(result, aes(x=date,y=da))+ylab("Data Availability [%]")
daFinalGraph<- dagraph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_y_continuous(breaks=seq(0, 115, 10))+
  scale_x_date(date_breaks = "1 month",date_labels = "%b")+
  ggtitle(paste("[SG-716] Singapore Data Availability"), subtitle = paste0("From ",date[1]," to ",date[length(date)]))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11)) +
  theme(axis.text.y = element_text(size=11), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5)) +
  theme(plot.title = element_text(face = "bold",size= 11,lineheight = 0.7,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)), panel.grid.minor = element_blank())+
  annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size=x,
           x = as.Date(date[round(0.518*length(date))]), y= 82)+
  annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)  "),size = x,
           x = as.Date(date[round(0.518*length(date))]), y= 77)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

daFinalGraph
ggsave(daFinalGraph,filename = paste0(pathWrite,"SG-716_DA_LC.pdf"),width =7.92, height =5)


gsiGraph <- ggplot(result, aes(x=date,y=gsi))+ylab("GHI [W/m�]")
gsiFinal_graph<- gsiGraph + geom_bar(stat = "identity", width = 1, position = "dodge")+
  theme_bw()+
  expand_limits(x=date[1],y=7.5)+
  scale_y_continuous(breaks=seq(0, 7, 1))+
  scale_x_date(date_breaks = "1 month",date_labels = "%b")+
  ggtitle(paste("[SG-716] Global Horizontal Irradiation Daily"), subtitle = paste0("From ",date[1]," to ",date[length(date)]))+
  theme(axis.title.y = element_text(face = "bold",size = 12,margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5)) +
  theme(plot.title = element_text(face = "bold",size= 11,lineheight = 0.8,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)), panel.grid.minor = element_blank())+
  geom_hline(yintercept=mean(result[,4][!is.na(result[,6])]),size=0.3,color = "red")+
  geom_hline(yintercept=4.47,size=0.3, colour="blue")+
  annotate("text",label = paste0("Current average daily GHI = ",round(mean(result[,4]),2)," kWh/m�.day    "),size=x,
           x = as.Date(date[round(0.518*length(date))]), y= 7.3, colour = "red")+
  annotate("text",label = paste0("Long term average daily GHI = 4.47 kWh/m�.day"),size=x,
           x = as.Date(date[round(0.518*length(date))]), y= 7.8, colour="blue")+
  annotate("text",label = paste0("Long term average annual GHI = 1632 kWh/m�  "),size=x,
           x = as.Date(date[round(0.518*length(date))]), y= 8.3)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))
gsiFinal_graph

ggsave(gsiFinal_graph,filename = paste0(pathWrite,"SG716_GSI_LC.pdf"),width =7.92, height =5)

pdf(graph3,width=7.92,height=5)

tamb <- result[,5]
hamb <- result[,6]

tamb[tamb <0.5] = NA
hamb[hamb <0.5] = NA
yaxis <- tamb
yaxis2 <- hamb

xaxis <- c(1:length(yaxis))
xaxis2 <- seq(1,(length(yaxis)),30)

par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, tamb, pch=4, axes=FALSE, ylim=c(-100,75), xlab="", ylab="", 
     type="l",col="orange", main=" ")
axis(2, ylim=c(-100,75),at = seq(-100,75,25),col="black",col.axis="black",las=1)
mtext(expression(bold("Tamb [�C]")),side=2,line=2.5,cex=1,las =3)

box()

## Allow a second plot on the same graph
par(new=TRUE)

## Plot the second plot and put axis scale on right
plot(xaxis, hamb, pch=3,  xlab="", ylab="", ylim=c(-0,250), 
     axes=FALSE, type="l", col="blue")

axis(4, ylim=c(-0,250), col="black",col.axis="black",las=1)
text(length(xaxis)*1.14,125,expression(paste(bold("Hamb [%]"))),xpd=NA,srt = -90, cex = 1)
title(paste("[SG-716] Daily Average Ambient Temperature \n From ", date[1], " to ", date[length(date)]), col="black", cex.main =1)

#mtext(expression(paste(bold("[SG-716] Daily Average Ambient Temperature and Relative Humidity"))),side=3,col="black",line=1.75,cex=1) 

####
#mtext(expression(paste(bold("From 2017-01-07 to 2018-05-16"))),side=3,col="black",line=0.75,cex=1)
####

text(length(xaxis)/2, 225, paste("Average Tamb =",format(round(mean(tamb[abs(tamb)<100&!is.na(tamb)]),1),nsmall = 1),"C"),cex = .8,col = "orange")
text(length(xaxis)/2, 35, paste("Average Hamb =",format(round(mean(hamb[abs(hamb)<100&!is.na(hamb)]),1),nsmall = 1),"%"),cex = .8,col = "blue")

axis(1,at = xaxis2,cex.axis = 0.7,labels = monthslabel)
legend(length(yaxis)*0.855,260,
       c("Tamb","Hamb"),
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),col=c("orange","blue")) # gives the legend lines the correct color and width

par(new=TRUE)

dev.off()

print(paste0("Graphs located @  ",pathWrite))

