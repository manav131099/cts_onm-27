source('/home/admin/CODE/common/aggregate.R')

registerMeterList("IN-015S",c("LV","HV","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 43 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = 26 #column for LastTime
aggColTemplate[5] = NA #column for Eac-1
aggColTemplate[6] = 22 #column for Eac-2
aggColTemplate[7] = NA #column for Yld-1
aggColTemplate[8] = 38 #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = 39 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("IN-015S","LV",aggNameTemplate,aggColTemplate)

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 43 #Column no for DA
aggColTemplate[3] = 25 #column for LastRead
aggColTemplate[4] = 26 #column for LastTime
aggColTemplate[5] = NA #column for Eac-1
aggColTemplate[6] = 23 #column for Eac-2
aggColTemplate[7] = NA #column for Yld-1
aggColTemplate[8] = 37 #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = 41 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("IN-015S","HV",aggNameTemplate,aggColTemplate)
