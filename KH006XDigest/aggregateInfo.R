source('/home/admin/CODE/common/aggregate.R')

registerMeterList("KH-006X",c("ComAp-01","ComAp-02","MSB-01-Solar","MSB-02-Solar","Sensors"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 3 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 5 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = 6 #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 7 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-006X","ComAp-01",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 3 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 5 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = 6 #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 7 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-006X","ComAp-02",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 9 #column for Yld-1
aggColTemplate[8] = 10 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 12 #column for PR-2
aggColTemplate[11] = 13 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-006X","MSB-01-Solar",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 9 #column for Yld-1
aggColTemplate[8] = 10 #column for Yld-2
aggColTemplate[9] = 11 #column for PR-1
aggColTemplate[10] = 12 #column for PR-2
aggColTemplate[11] = 13 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-006X","MSB-02-Solar",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 6 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = NA #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = NA #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 2 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] =  3 #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-006X","Sensors",aggNameTemplate,aggColTemplate)

